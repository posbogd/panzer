﻿using System;
using UnityEngine;

public interface IWeapon{
	void AddWeapon(Transform position);
	void Shoot ();
}

