﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : Node {


	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.W))
			Emit ("move-front");
		if (Input.GetKey (KeyCode.S))
			Emit ("move-back");
		if (Input.GetKey (KeyCode.A))
			Emit ("move-left");
		if (Input.GetKey (KeyCode.D))
			Emit ("move-right");
		if (Input.GetKey (KeyCode.LeftArrow))
			Emit ("rotate-left");
		if (Input.GetKey (KeyCode.RightArrow))
			Emit ("rotate-right");
		if (Input.GetKey (KeyCode.UpArrow))
			Emit ("rotate-tower-left");
		if (Input.GetKey (KeyCode.DownArrow))
			Emit ("rotate-tower-right");
		if (Input.GetKeyDown (KeyCode.X))
			Emit ("hold-fire");
		if (Input.GetKeyUp (KeyCode.X))
			Emit ("stop-fire");
		if (Input.GetKeyUp (KeyCode.X))
			Emit ("stop-fire");
		if (Input.GetKeyUp (KeyCode.Q))
			Emit ("pre-weapon");
		if (Input.GetKeyUp (KeyCode.E))
			Emit ("next-weapon");
		
	}
}
