﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
public class PlayerModel : MonoBehaviour {
//	public float moveSpeed = 2f;


	public Globals.WeaponsTypes weaponType;
	IWeapon iWeapon;
	public GameObject weapon;
	[SerializeField]
	public PlayerTank tank = new PlayerTank(100f,0.3f,3f);

	// Use this for initialization
	void Start () {
		HandleWeaponType ();
		Transform t = transform.GetChild (1).GetComponent<Transform>();
		PlayerController.On ("move-front", () => {
			transform.Translate (Vector3.forward * Time.deltaTime * tank.speed);
		});
		PlayerController.On ("move-back", () => {
			transform.Translate (Vector3.back * Time.deltaTime * tank.speed);
		});
		PlayerController.On ("move-left", () => {
			transform.Translate (Vector3.left * Time.deltaTime * tank.speed);
		});
		PlayerController.On ("move-right", () => {
			transform.Translate (Vector3.right * Time.deltaTime * tank.speed);
		});
		PlayerController.On ("rotate-left", () => {
			transform.Rotate (0,-40*tank.speed*Time.deltaTime,0);
		});
		PlayerController.On ("rotate-right", () => {
			transform.Rotate (0,40*tank.speed*Time.deltaTime,0);
		});
		PlayerController.On ("rotate-tower-left", () => {
			t.Rotate (0,-40*tank.speed*Time.deltaTime,0);
		});
		PlayerController.On ("rotate-tower-right", () => {
			t.Rotate (0,40*tank.speed*Time.deltaTime,0);
		});
		PlayerController.On ("hold-fire", () => {
			InvokeRepeating ("Fire", tank.fireRate, tank.fireRate);
			//Fire();
		});
		PlayerController.On ("stop-fire", () => {
			CancelInvoke ();
		});
		PlayerController.On ("pre-weapon", () => {
			if((int)weaponType-1>=0)
			{
				weaponType=(Globals.WeaponsTypes)((int)weaponType-1);
			}else 
				weaponType=(Enum.GetValues(typeof(Globals.WeaponsTypes)).Cast<Globals.WeaponsTypes>().Last());
			HandleWeaponType();
		});
		PlayerController.On ("next-weapon", () => {
			if((int)weaponType+1<=(int)(Enum.GetValues(typeof(Globals.WeaponsTypes)).Cast<Globals.WeaponsTypes>().Last()))
			{
				weaponType=(Globals.WeaponsTypes)((int)weaponType+1);
			}else 
				weaponType=(Globals.WeaponsTypes)0;
			HandleWeaponType();
		});


	}
	public void HandleWeaponType()
	{  
		Component c = gameObject.GetComponent<IWeapon> ()as Component;
		if (c != null) {
			Destroy (c);
			Destroy (weapon);
		}
		#region Strategy
		switch (weaponType)
		{
		case Globals.WeaponsTypes.Single:
			iWeapon=gameObject.AddComponent<SingleBulletLauncher>();
			break;
		case Globals.WeaponsTypes.Doble:
			iWeapon=gameObject.AddComponent<DobleBulletLauncher>();
			break;
		default:
			iWeapon=gameObject.AddComponent<SingleBulletLauncher>();
			break;
		}
		iWeapon.AddWeapon(transform);
		#endregion
	}

	public void Fire()
	{	
		iWeapon.Shoot ();
	}

}
