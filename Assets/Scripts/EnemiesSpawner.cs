﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemiesSpawner : MonoBehaviour {
	public float timeToSpawn=3f;
	int enemiesCount;
	public GameObject spawnPoint;
	float nextSpawn;
	void Start(){
		nextSpawn = Time.time+timeToSpawn;
		enemiesCount = 0;
	}
	void Update(){
		if (Time.time > nextSpawn&&enemiesCount<=10) {
			transform.rotation = Quaternion.Euler (0,Random.Range(0,359),0);
			GameObject enemyObj = Instantiate (Resources.Load ("EnemyPrefab", typeof(GameObject)))as GameObject;
			enemyObj.transform.position = spawnPoint.transform.position;
			nextSpawn = Time.time + timeToSpawn;
		}
	}


}
