﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {
	public float speed;
	public int dmg;
	void Start(){
		dmg = 101;
	}
	//public Vector3 direction;
	void Update()
	{
		transform.Translate (transform.forward*speed,Space.World);
	}
	void OnEnable(){
		Invoke ("Hide",2f);
	}
	void Hide(){
		gameObject.SetActive (false);
	}
	void OnDisable(){
		CancelInvoke ();
	}
	void OnCollisionEnter(Collision coll)
	{    Debug.Log (coll.transform.tag);
	     if (coll.transform.tag == "Enemy") {
			coll.transform.GetComponent<EnemyModel> ().enemy.TakeDamage (dmg);
			//TODO Better realization

			if (coll.transform.GetComponent<EnemyModel> ().enemy.hitPoints <= 0)
				Destroy (coll.gameObject);
			Debug.Log (coll.transform.GetComponent<EnemyModel> ().tank.hitPoints);
			Hide ();

		}
	}
}

