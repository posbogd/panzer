﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {
	public GameObject tank;
	// Use this for initialization
	Vector3 offset;
	void Start()
	{
		offset = transform.position - tank.transform.position;
	}
	// Update is called once per frame
	void LateUpdate () {
		transform.position=tank.transform.position + offset;
	}
}
