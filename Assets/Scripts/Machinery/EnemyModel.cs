﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyModel : MonoBehaviour {
	public GameObject cubAva;
	public GameObject ballAva;
	public GameObject combinedAva;
	public Machinery enemy;
   GameObject playerTank;
	[SerializeField]
	public PlayerTank tank = new PlayerTank(100f,0.3f,2f);
	// Use this for initialization
	void Start () {
		playerTank = GameObject.FindGameObjectWithTag ("Player");
		enemy = MonstersFactory.getMonster ((Globals.EnemyesTypes)(Random.Range(0,3)));
		SelectAva (enemy.type);
	}
	void Update()
	{
		transform.Translate (
			(playerTank.transform.position-this.transform.position).normalized*enemy.speed*Time.deltaTime
		);
	}
	//void OnCollider
	void SelectAva(Globals.EnemyesTypes type )
	{
		switch(type){
		case Globals.EnemyesTypes.Ball:
			{
				ballAva.SetActive (true);
				combinedAva.SetActive (false);
				cubAva.SetActive (false);
			}
			break;
		case Globals.EnemyesTypes.Cube:
			{
				ballAva.SetActive (false);
				combinedAva.SetActive (false);
				cubAva.SetActive (true);
			}
			break;
		case Globals.EnemyesTypes.Combined:
			{
				ballAva.SetActive (false);
				combinedAva.SetActive (true);
				cubAva.SetActive (false);
			}
			break;
		}
	}
	void OnCollisionEnter(Collision coll)
	{    Debug.Log (coll.transform.tag);
		if (coll.transform.tag == "Player") {
			coll.transform.GetComponent<PlayerModel> ().tank.TakeDamage (50);
			//TODO Better realization
			if (coll.transform.GetComponent<PlayerModel> ().tank.hitPoints <= 0)
			Destroy (coll.gameObject);
			Debug.Log (coll.transform.GetComponent<PlayerModel> ().tank.hitPoints);
			//TODO Implement Object pulling
			Destroy (gameObject);
		}
	}
}
