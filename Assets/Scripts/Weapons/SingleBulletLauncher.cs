﻿using System;
using UnityEngine;

public class SingleBulletLauncher:MonoBehaviour,IWeapon 
{
	public Vector3 weaponPosition;
	public Transform firstBulletSpawn;
	public void AddWeapon(Transform tankTransform)
	{
		GameObject bulletLauncher = Instantiate (Resources.Load ("SingleBulletLauncher", typeof(GameObject)))as GameObject;
		this.gameObject.GetComponent<PlayerModel> ().weapon = bulletLauncher;
		bulletLauncher.transform.SetParent (transform);
		bulletLauncher.transform.position =new Vector3( tankTransform.position.x, tankTransform.position.y+0.5f, tankTransform.position.z);
		bulletLauncher.transform.rotation = tankTransform.rotation;
	
		weaponPosition = tankTransform.position;
		firstBulletSpawn = bulletLauncher.transform.GetChild (0);

	}
	public void Shoot (){
		GameObject obj = ObjectPulling.current.GetPooledObject ();
		if (obj == null)
			return;

		obj.transform.position = firstBulletSpawn.position;
		obj.transform.rotation = firstBulletSpawn.rotation;
		obj.SetActive (true);

		//		GameObject bullet1 = Instantiate (Resources.Load ("Bullet", typeof(GameObject)))as GameObject;
		//		bullet1.transform.position = firstBulletSpawn;
		//		GameObject bullet2 = Instantiate (Resources.Load ("Bullet", typeof(GameObject)))as GameObject;
		//		bullet2.transform.position = secondBulletSpawn;
	}
}


