﻿using System;
using UnityEngine;

public class DobleBulletLauncher:MonoBehaviour,IWeapon
{
	public Vector3 weaponPosition;
	Transform firstBulletSpawn;
	Transform secondBulletSpawn;
	public void AddWeapon(Transform tankTransform)
	{
		GameObject bulletLauncher = Instantiate (Resources.Load ("DoubleBulletLauncher", typeof(GameObject)))as GameObject;
		this.gameObject.GetComponent<PlayerModel> ().weapon = bulletLauncher;
		bulletLauncher.transform.SetParent (transform);
		bulletLauncher.transform.position =new Vector3( tankTransform.position.x, tankTransform.position.y+0.5f, tankTransform.position.z);
		bulletLauncher.transform.rotation = tankTransform.rotation;
		weaponPosition = tankTransform.position;
		firstBulletSpawn = bulletLauncher.transform.GetChild (0);
		secondBulletSpawn  = bulletLauncher.transform.GetChild (1);
	}
	public void Shoot (){
		GameObject obj1 = ObjectPulling.current.GetPooledObject ();

		if (obj1 == null)
			return;
		obj1.transform.position = firstBulletSpawn.position;
		obj1.transform.rotation = firstBulletSpawn.rotation;
		obj1.SetActive (true);

		GameObject obj2 = ObjectPulling.current.GetPooledObject ();
		if (obj2 == null)
			return;
		obj2.transform.position = secondBulletSpawn.position;
		obj2.transform.rotation = secondBulletSpawn.rotation;
		obj2.SetActive (true);

//		GameObject bullet1 = Instantiate (Resources.Load ("Bullet", typeof(GameObject)))as GameObject;
//		bullet1.transform.position = firstBulletSpawn;
//		GameObject bullet2 = Instantiate (Resources.Load ("Bullet", typeof(GameObject)))as GameObject;
//		bullet2.transform.position = secondBulletSpawn;
	}

}

