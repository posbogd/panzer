﻿public abstract class Machinery  {
	public float hitPoints;
	public float armor;
	public float speed;
	public Globals.EnemyesTypes type;
	abstract public void TakeDamage (float dmg);
}
public class Monster:Machinery,IDamagble{
	public Monster(float hitPoints,float armor,float speed){
		this.hitPoints = hitPoints;
		this.armor = armor;
		this.speed = speed;
	}
	public override void TakeDamage(float dmg)
	{
		hitPoints = hitPoints - dmg * armor;
	}
}
public class PlayerTank:Machinery,IDamagble{
	public float fireRate;
	public PlayerTank(float hitPoints,float armor,float speed){
		this.hitPoints = hitPoints;
		this.armor = armor;
		this.speed = speed;
		this.fireRate = 0.5f;
	}
	public override void TakeDamage(float dmg)
	{
		hitPoints = hitPoints - dmg * armor;
	}
}

public class MonsterCube:Machinery,IDamagble{
	public float fireRate;

	public MonsterCube(){
		this.hitPoints = 110f;
		this.armor = 1f;
		this.speed = 2f;
		this.type = Globals.EnemyesTypes.Cube;
	}
	public override void TakeDamage(float dmg)
	{
		hitPoints = hitPoints - dmg * armor;
	}
}
public class MonsterBall:Machinery,IDamagble{
	public float fireRate;

	public MonsterBall(){
		this.hitPoints = 20f;
		this.armor = 1f;
		this.speed = 5f;
		this.type = Globals.EnemyesTypes.Ball;
	}
	public override void TakeDamage(float dmg)
	{
		hitPoints = hitPoints - dmg * armor;
	}
}
public class MonsterCombined:Machinery,IDamagble{
	public float fireRate;

	public MonsterCombined(){
		this.hitPoints = 250f;
		this.armor = 1f;
		this.speed =3f;
		this.type =Globals.EnemyesTypes.Combined;
	}
	public override void TakeDamage(float dmg)
	{
		
		hitPoints = hitPoints - dmg * armor;
	}
}
public static class MonstersFactory{
	public static Machinery getMonster (Globals.EnemyesTypes type)
	{
		switch(type){
		case Globals.EnemyesTypes.Ball:
			return new MonsterBall();
		case Globals.EnemyesTypes.Combined:
			return new MonsterCombined();
		case Globals.EnemyesTypes.Cube:
			return new MonsterCube();
		default:return null;
		}
	}
}
